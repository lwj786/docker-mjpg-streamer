FROM debian

RUN set -eux; \
    \
    buildDeps=" \
        git \
        ca-certificates \
        make \
        cmake \
        gcc \
        g++ \
    "; \
    Deps=" \
        libjpeg-dev \
    "; \
    \
    apt-get update && apt-get install -y --no-install-recommends $buildDeps $Deps; \
    rm -rf /var/lib/apt/lists/*; \
    \
    MJPG_STREAMER_REPO="https://github.com/jacksonliam/mjpg-streamer"; \
    git clone $MJPG_STREAMER_REPO; \
    \
    cd ./mjpg-streamer/mjpg-streamer-experimental/; \
    make && make install; \
    ln -s /usr/local/share/mjpg-streamer/www ../../webcam; \
    \
    cd - && rm -rf ./mjpg-streamer; \
    apt purge -y --auto-remove $buildDeps

EXPOSE 8080
ENV LD_LIBRARY_PATH /usr/local/lib/mjpg-streamer

ENTRYPOINT ["/usr/local/bin/mjpg_streamer"]
CMD ["-i", "input_uvc.so", "-o", "output_http.so -w webcam -c user:password"]
